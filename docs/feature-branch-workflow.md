# Feature Branch Workflow
- via the command line by Miko <jose.suriajr@metrobank.com.ph. 
- For steps using VS Code, please consult Jiro <steven.bonsol@metrobank.com.ph>. 
- For questions and feedback, please hunt down Miko <jose.suriajr@metrobank.com.ph>. 
- Disclaimer: this document will continuously be updated.

### 1. Branch naming
- As a rule, sa 'develop' branch tayo mag-checkout
- Use 'feature/webta-(id ng ticket)-(short title ng ticket with dashes)'
```sh
$ git checkout develop
$ git checkout -b feature/webta-16-create-initial-codebase-automation-repository
```
### 2. Adding, committing and pushing your feature branch
- See sample below. Egs., ticket is "WEBTA-16 Create Initial Codebase for Automation Repository"
```sh
# Do your changes, coding, etc.
# When ready to push to remote for PR
$ git add .  # or specify your files
$ git status # to display yung ni-add nyo for commit
$ git commit -m "WEBTA-16 added kyeme feature to charot component"

# Before pushing, make sure that:
# a. Ticket is assigned to YOURSELF
#    - If not, ask the ticket owner to assign the ticket to you
#    - May cases kasi na we need to work on the same feature branch
# b. Status is not OPEN (must be IN PROGRESS)
#    - For now, wala tayo permission to set the ticket status (ask Sir Rye)
# c. SR Number is provided
#    - Yung PID sa time tracker natin: PID-004384
# d. Fix version is set
#    - Just select the provided value na nakalagay sa dropdown.
# Mag-error/haharangin ni git ang push nyo if the above are not met.

# If ever gusto nyo palitan yung commit message (after mo na-commit)
$ git commit --amend -m "New commit message"

# Initial push, para ma-create yung feature branch sa remote
$ git push -u origin <your feature branch>

# For succeeding updates to your remote (during PR/MR code review)
$ git push


# If you need to rename your branch
$ git checkout <branch you want to rename>
$ git branch -m <name ng new branch, still following format>
```
### 3. Making your Pull Request (PR)/Merge Request (MR)
- Go to the link in the displayed successful git message, nasa page below:
[Merge Requests](https://epma-itg.dopsglab01.mbtc.mgc.local/itg/webta/web-test-automation/-/merge_requests)
- Look for your new feature branch, click nyo 'Merge Request'. 
[Instructions](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#from-an-issue)
- Accomplish the form, assign to Miko. Sample format ng description below:
```sh
1. Added tests for sign up page
2. Refactored commands, removed comments
3. Etc.
```
- Submit form. Copy link of PR/MR and paste in our new [announcements chat group](https://metrobankvendors.workplace.com/groups/2123622461362617)

### 4. Coding Guidelines
- Please refer to this [document from Sir Chris Torres](https://l.workplace.com/l.php?u=https%3A%2F%2Fcdn.fbsbx.com%2Fv%2Ft59.2708-21%2F356646658_238706715681207_7889095901889269164_n.pdf%2Fcoding-guidelines-chris-torres.pdf%3F_nc_cat%3D108%26ccb%3D1-7%26_nc_sid%3D0cab14%26_nc_ohc%3DyG_WlGAuIKAAX_LIXwx%26_nc_ht%3Dcdn.fbsbx.com%26uss%3D942dacea8c5509df%26odm%3DbWV0cm9iYW5rdmVuZG9ycy53b3JrcGxhY2UuY29t%26oe2%3D649C833E%26oh%3D03_AdS0aBqy2-ya39CL8osvTqK1Y3Qsee7obJoigz8FLUuCtA%26oe%3D649AB32E%26dl%3D1&h=AT0PGhssJxrSxjd4ieVNGK_5N5z-7mxgHBoLJjjFAQveUS3munmWvuAzPCNCJgQx-q6s6u1rNaGjdCRf0iejJksuhtEArv-ckofCVr_RpylHDcbEDnHfg0hf9fgOzsdSuu63tgF5FN9aXFrFfQ) 
